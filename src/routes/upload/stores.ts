import { type Writable, writable } from 'svelte/store';

export const files: Writable<Blob[]> = writable([]);
export const fileData: Writable<string[][]> = writable([]);
export const headings: Writable<{ heading: string; index: number }[]> = writable([]);
export const rows: Writable<string[][]> = writable([]);
export const neededColumns: Writable<{ heading: string; index: number }[]> = writable([]);
