# %%
import duckdb as sql
import numpy as np
import pandas as pd

# %%
employee_frame = pd.read_excel('upload/sam_set_employee.xlsx')
print(employee_frame)

# %%
timesheets_frame = pd.read_excel('upload/sam_set_timesheets.xlsx')
print(timesheets_frame)

# %%
count_employees_from_timesheets = """
    SELECT
        COUNT(DISTINCT "Employee ID") as "Total Employees"
    FROM
        timesheets_frame
"""
sql.query(count_employees_from_timesheets).df()

# %%
count_shifts = """
    SELECT
        COUNT() as "Samskie's Shifts"
    FROM
        timesheets_frame
    WHERE
        "Employee Name" == 'Samskies Shapiro'
"""
sql.query(count_shifts).df()

# %%
count_meal_breaks = """
    SELECT
        COUNT() as "Samskie's Meal Breaks"
    FROM
        timesheets_frame
    WHERE
        "Punch Out" IS NOT NULL
"""
sql.query(count_meal_breaks).df()

# %%
count_meal_breaks_missing = """
    SELECT
        COUNT() as "Samskie's Skipped Meal Breaks"
    FROM
        timesheets_frame
    WHERE
        "Punch Out" IS NULL
"""
sql.query(count_meal_breaks_missing).df()

# %%
count_meal_breaks_late = """
    SELECT
        COUNT() as "Samskie's Late Meal Breaks"
    FROM
        timesheets_frame
    WHERE
        date_diff('minute', "Punch In", "Punch Out") > 5 * 60
"""
sql.query(count_meal_breaks_late).df()

# %%
count_meal_breaks_short = """
    SELECT
        COUNT() as "Samskie's Short Meal Breaks"
    FROM
        timesheets_frame
    WHERE
        date_diff('minute', "Punch Out", "Punch In.1") < 30
"""
sql.query(count_meal_breaks_short).df()

# %%
count_employees_from_employees = """
    SELECT
        COUNT(DISTINCT "Employee ID") as "Total Employees"
    FROM
        employee_frame
"""
sql.query(count_employees_from_employees).df()

# %%
count_employees_active = """
    SELECT
        COUNT(DISTINCT "Employee ID") as "Total Employees"
    FROM
        employee_frame
    WHERE
        Status = 'Active'
"""
sql.query(count_employees_active).df()

# %%
count_employees_terminated = """
    SELECT
        COUNT(DISTINCT "Employee ID") as "Total Employees"
    FROM
        employee_frame
    WHERE
        Status = 'Term'
"""
sql.query(count_employees_terminated).df()

# %%
employee_workweeks = """
    SELECT
        "Employee Name",
        CASE
            WHEN date_diff('day', make_date(2024, 1, 1), "Hire Date") > 0
            THEN "Hire Date"::date
            ELSE make_date(2024, 1, 1)
        END as "start_of_year",
        date_trunc('week', make_date(2024, 1, 4))::date as "first_sunday",
        CASE
            WHEN "Term Date" IS NOT NULL
            THEN "Term Date"::date
            ELSE today()
        END as "last_day",
        "Hours Worked in 2024",
        greatest(ceil(("last_day" - "first_sunday") / 7), 0) as "weeks_by_date",
        ceil("Hours Worked in 2024" / 40) as "weeks_by_hour"
    FROM
        employee_frame
"""
sql.query(employee_workweeks).df()

# %%
