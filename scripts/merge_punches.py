# %%

import pandas as pd

from os import listdir
from pathlib import Path

input_dir = Path("input")
output_dir = Path("output")
output_file = output_dir.joinpath("timesheet_data.xlsx")
columns = ["Employee Code", "Date", "In", "Out"]
new_shift_threshold = pd.Timedelta(hours=4)

# %% Walk the input directory and concatenate all the raws together

paths = [input_dir.joinpath(file) for file in listdir(input_dir)]
dataframes: list[pd.DataFrame] = []
for path in paths:
    dataframes.append(pd.read_excel(path)[columns])

df = pd.concat(dataframes)

# %% Filter out incomplete punch data

df = df[(df["In"].notna() & df["In"].notnull())
        & (df["Out"].notna() & df["Out"].notnull())]

# %% Add timestamps

df["Start Time"] = pd.to_datetime(df["Date"].astype(str) + " " + df["In"])
df["End Time"] = pd.to_datetime(df["Date"].astype(str) + " " + df["Out"])

# %% Calculate time since the last shift ended

df["Last Shift"] = df.groupby(["Employee Code"])["End Time"].shift(1)
df["Time Away"] = df["Start Time"] - df["Last Shift"]

# %% Group shifts into consecutive shifts

df["Contiguous"] = df["Time Away"] < new_shift_threshold
df["Shift ID"] = (df["Contiguous"] == False).cumsum()
df["Shift Number"] = (df.groupby(df["Shift ID"]).cumcount()) + 1

# %% Rearrange table by shift groups

max_shifts = df["Shift Number"].max()
shift_dataframes: list[pd.DataFrame] = []
final_columns = ["Employee Code", "Shift ID", "Date"]
for i in range(max_shifts):
    i += 1
    rename_columns = {
        "Start Time": f"Start Time {i}",
        "End Time": f"End Time {i}",
    }
    df_i = df[df["Shift Number"] == i]
    if i != 1:
        df_i = df_i[["Shift ID", "Start Time", "End Time"]]
    shift_dataframes.append(df_i.rename(columns=rename_columns))
    final_columns.append(f"Start Time {i}")
    final_columns.append(f"End Time {i}")

left = shift_dataframes.pop(0)
while shift_dataframes:
    right = shift_dataframes.pop(0)
    left = left.merge(right, how="left", on="Shift ID")
final = left[final_columns]

# %% Dump to Excel

output_dir.mkdir(exist_ok=True)
final.to_excel(output_file)

# %%
